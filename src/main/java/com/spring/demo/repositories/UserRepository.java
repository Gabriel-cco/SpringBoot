package com.spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.spring.demo.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {
	

}
