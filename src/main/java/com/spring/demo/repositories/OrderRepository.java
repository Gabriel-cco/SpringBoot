package com.spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.spring.demo.entities.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
	

}
