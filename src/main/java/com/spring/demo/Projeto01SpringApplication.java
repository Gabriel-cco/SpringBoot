package com.spring.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Projeto01SpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(Projeto01SpringApplication.class, args);
	}

}
